FROM maven:3.6.3-openjdk-11-slim as builder
WORKDIR /project
ADD . ./

RUN mkdir -p /root/.m2 \
    && mkdir /root/.m2/repository
# Copy maven settings, containing repository configurations
COPY settings.xml /root/.m2

RUN mvn -q clean package -Dmaven.test.skip=true

MAINTAINER baeldung.com
FROM adoptopenjdk/openjdk11:alpine-jre
ARG NRA_VERSION=6.1.0
COPY --from=builder project/target/app.jar ./app.jar

CMD ["java", "--enable-preview", "-jar", "app.jar"]