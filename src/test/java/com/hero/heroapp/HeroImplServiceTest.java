package com.hero.heroapp;

import com.hero.heroapp.service.HeroImplService;
import com.hero.heroapp.service.HeroService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
public class HeroImplServiceTest {

    @Mock
    private HeroRepository mockHeroRepository;

    private Hero hero;

    private HeroService heroService;

    @Before
    public void setUp() {
        initMocks(this);
        heroService = new HeroImplService(mockHeroRepository);
        hero = Hero.builder()
                .id(1)
                .name("Gustavo")
                .build();

        Mockito.when(mockHeroRepository.save((Hero) any()))
                .thenReturn(hero);
        Mockito.when(mockHeroRepository.findById(anyInt()))
                .thenReturn(Optional.of(hero));
    }

    @Test
    public void testUpdateHeroOk() {
        final String name = "Gustavo";
        Hero result = heroService.saveHero(new HeroRequest(1,"Gustavo"));
        assertEquals(name, result.getName());
    }


    // continuing with other tests
}
