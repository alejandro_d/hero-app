package com.hero.heroapp;

import com.hero.heroapp.service.HeroImplService;
import com.hero.heroapp.service.HeroService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
public class HeroIntegrationTest {

    private HeroRepository mockHeroRepository;

    private Hero hero;

    private HeroService heroService;

    @Before
    public void setUp() {
        initMocks(this);
        heroService = new HeroImplService(mockHeroRepository);
        hero = Hero.builder()
                .id(1)
                .name("Titan")
                .powers(Arrays.asList(new Power(1L,PowerEnum.Flight,100)))
                .build();
    }

    @Test
    public void testSaveAndUpdateHeroOk() {
        final String name = "Titan";
        Hero result = heroService.saveHero(new HeroRequest(1,"Titan"));
        assertEquals(name, result.getName());
        Hero hero2 = heroService.findById(1);
        assertEquals(hero.getName(),hero2.getName());
    }

    @Test(expected = NoSuchElementException.class)
    public void testNotFoundHero() {
        Hero hero2 = heroService.findById(122);
    }


    // continuing with other tests
}

