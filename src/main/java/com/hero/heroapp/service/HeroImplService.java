package com.hero.heroapp.service;

import com.hero.heroapp.Hero;
import com.hero.heroapp.HeroRepository;
import com.hero.heroapp.HeroRequest;
import com.hero.heroapp.error.HeroDoesNotExistException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log
@Service
public class HeroImplService implements  HeroService{

    HeroRepository repository;
    
    @Autowired
    public HeroImplService(HeroRepository heroRepository){
        repository = heroRepository;
    }

    @Override
    public Hero findById(int id) {
        return repository.getById(id);
    }

    @Override
    @Cacheable(cacheNames="heroes", condition="#id > 1")
    public List<Hero> findByPlace(String place) {
        log.info("Message");
        return repository.findByNameIgnoreCaseContaining(place);
    }

    @Override
    @CacheEvict(cacheNames="heroes", allEntries=true)
    public void flushCache() {
    }

    @Override
    public Hero findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Hero saveHero(HeroRequest request) {
        Optional<Hero> heroOptional = repository.findById(request.getId());
        heroOptional.ifPresent(hero -> repository.save(update(fillHero(hero, request))));
        return heroOptional.orElseThrow();
    }

    private Hero fillHero(Hero hero, HeroRequest request) {
        hero.setName(request.getName());
        return hero;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    @CachePut(cacheNames="heroes", key="#hero.id")
    public Hero update(Hero hero) {
        Optional<Hero> invoiceOptional=repository.findById(hero.getId());
        if (! invoiceOptional.isPresent()){
            throw new HeroDoesNotExistException(hero.getId());
        }
        return hero;
    }

}
