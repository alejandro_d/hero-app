package com.hero.heroapp.service;

import com.hero.heroapp.Hero;
import com.hero.heroapp.HeroRequest;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface HeroService {
    Hero findById(int id);

    @Cacheable(cacheNames="heroes", condition="#id > 1")
    List<Hero> findByPlace(String place);

    @CacheEvict(cacheNames="heroes", allEntries=true)
    void flushCache();

    Hero findByName(String name);

    Hero saveHero(HeroRequest request);

    void delete(int id);
}
