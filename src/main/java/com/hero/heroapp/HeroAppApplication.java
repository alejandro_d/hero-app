package com.hero.heroapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HeroAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeroAppApplication.class, args);
    }

}
