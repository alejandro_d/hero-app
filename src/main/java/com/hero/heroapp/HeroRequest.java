package com.hero.heroapp;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class HeroRequest {

    private Integer id;
    private String name;
}
