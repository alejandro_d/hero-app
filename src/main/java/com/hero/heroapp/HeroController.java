package com.hero.heroapp;

import com.hero.heroapp.service.HeroImplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/hero")
@Api(value="hero", description="Operations with heroes")
public class HeroController {

    @Autowired
    private HeroImplService heroService;

    @GetMapping(path="/{id}")
    public Hero findById(@RequestParam int id){
        return heroService.findById(id);
    }

    @GetMapping(path="/{place}")
    public List<Hero> findById(@RequestParam String place){
        return heroService.findByPlace(place);
    }


    @ApiOperation(value = "create hero",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully"),
            @ApiResponse(code = 406, message = "You are not able to create hero")})
    @PutMapping(path = "/new")
    public ResponseEntity<Void> createNewHero(@RequestBody HeroRequest request) {
        Hero hero = heroService.findByName(request.getName());
        if (hero != null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
        } else {
            heroService.saveHero(request);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
    }

    @DeleteMapping(path = "/{id}")
    public void deleteHero(@RequestParam int id){
        heroService.delete(id);
    }
}
