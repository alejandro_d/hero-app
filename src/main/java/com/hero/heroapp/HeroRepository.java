package com.hero.heroapp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroRepository extends JpaRepository<Hero, Integer> {

    List<Hero> findByNameIgnoreCaseContaining(String place);

    Hero findByName(String name);
}
