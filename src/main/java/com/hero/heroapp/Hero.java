package com.hero.heroapp;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Hero {

    @Id
    private int id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "power")
    private List<Power> powers;
}
