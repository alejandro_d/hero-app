package com.hero.heroapp.error;

import lombok.Getter;

@Getter
public class HeroDoesNotExistException extends RuntimeException {

    private String message;

    public HeroDoesNotExistException(int id) {
        message = "Hero does not exist with id :"+ id;
    }
}
