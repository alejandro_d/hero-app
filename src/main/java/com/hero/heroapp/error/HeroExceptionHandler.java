package com.hero.heroapp.error;

import com.hero.heroapp.HeroController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.NoSuchElementException;

@ControllerAdvice(assignableTypes = HeroController.class)
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class HeroExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeroExceptionHandler.class);

    @ExceptionHandler(HeroDoesNotExistException.class)
    public ResponseEntity handleFirstShotNotFound(HeroDoesNotExistException ex) {
        LOGGER.error("handleHeroShotNotFound", ex);
        return new ResponseEntity(new ErrorValidation("Hero Error"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity handleNoSuchElemen(NoSuchElementException  ex) {
        LOGGER.error("handleNoSuchElemen", ex);
        return new ResponseEntity(new ErrorValidation("Hero not found"), HttpStatus.NOT_FOUND);
    }
}
