package com.hero.heroapp;

public enum PowerEnum {

    Telekinesis("Telekinesis"),
    Shapeshifting("Shapeshifting"),
    Healing("Healing"),
    Elemental_Control("Elemental Control"),
    Mind_Control("Mind Control"),
    Super_Speed("Super Speed"),
    Flight("Flight");

    public final String label;

    private PowerEnum(String label) {
        this.label = label;
    }
}
