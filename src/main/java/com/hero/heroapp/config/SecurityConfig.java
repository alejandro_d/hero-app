package com.hero.heroapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/hero/**")
                .hasAuthority("SCOPE_read")
                .antMatchers(HttpMethod.POST, "/hero/**")
                .hasAuthority("SCOPE_write")
                .antMatchers(HttpMethod.PUT, "/hero/**")
                .hasAuthority("SCOPE_create")
                .antMatchers(HttpMethod.DELETE, "/hero/**")
                .hasAuthority("SCOPE_erase")
                .anyRequest()
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt();
    }
}
